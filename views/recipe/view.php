<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Usermodel;
use app\models\Review;
/* @var $this yii\web\View */
/* @var $model app\models\Recipe */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Recipes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="recipe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'ingredients:ntext',
            'image:ntext',
            'category',
            'cooking_time',
            'posted_by',
        ],
    ]) ?>

</div> -->
<?php
$user = Usermodel::find()
            ->where(['=', 'id', $model->posted_by])
            ->one();
$reviews = Review::find()
            ->where(['=', 'recipe_id', $model->id])
            ->All();

?>
<div class="agileinfo_single">
    <div class="col-md-12">
    <h5><?php echo $model->name;?></h5>
    <input type="hidden" id="recipe_id" value="<?php echo $model->id;?>">
    <div class="col-md-4 agileinfo_single_left">
        <img id="recipe_img"  src="<?php echo Yii::$app->homeUrl?>uploads/<?php echo $model->image;?>" alt=" " class="img-responsive" />
    </div>
    <div class="col-md-8 agileinfo_single_right">
        <div class="posted_by">
            <span class="fa fa-user"></span>
            <span><?php echo $user->first_name.' '.$user->last_name;?></span>
        </div>
        <div class="posteddate">
            <?php echo $model->created_date;?>
        </div>
        
        
        <div class="w3agile_description">
            <h4>Ingredients :</h4>
            <?php echo $model->ingredients;?>
        </div>
        <div class="w3agile_description">
            <h4>Cooking Time :</h4>
            <?php echo $model->cooking_time;?>
        </div>
    </div>
    </div>
    <div class="col-md-12">
        <div class="w3agile_description">
            <h4>Description :</h4>
            <?php echo $model->description;?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="w3agile_description reviews">
            <h4>Rating & Review :</h4>
            <div class="rating_div">
                <?php foreach($reviews as $key=>$review):?>
                    <?php if($review->rating==5):?>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <?php elseif($review->rating==4):?>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <?php elseif($review->rating==3):?>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <?php elseif($review->rating==2):?>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <?php elseif($review->rating==1):?>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <?php endif;?>
                    <br>
                    <p><?php echo $review->feedback;?></p>
                    <?php endforeach;?>
            </div>
            <!-- <div class="review_desc">
                    Very Bad recipe...
                </div> -->
            
        </div>
        <div class="w3agile_description reviews write_review_desc">
            <h4>Write Review :</h4>
            <div class="write_review review_desc">
                <textarea id="review_desc" name="review_desc" class="form-control"></textarea>
            </div>
            <div id="reviewStars-input">
                <input id="star-4" type="radio" name="reviewStars" value="5"/>
                <label title="gorgeous" for="star-4"></label>

                <input id="star-3" type="radio" name="reviewStars" value="4"/>
                <label title="good" for="star-3"></label>

                <input id="star-2" type="radio" name="reviewStars" value="3"/>
                <label title="regular" for="star-2"></label>

                <input id="star-1" type="radio" name="reviewStars" value="2"/>
                <label title="poor" for="star-1"></label>

                <input id="star-0" type="radio" name="reviewStars" value="1"/>
                <label title="bad" for="star-0"></label>
            </div>
        </div>
        <input type="hidden" id="write_review_action" value="<?php echo Yii::$app->homeUrl?>recipe/writereview">
    </div>
    <div class="w3agile_description row write_btn">
        <div class="col-md-12">
            <div class="col-md-4">
                <button type="button" id="btn-write-review" class="btn btn-primary">Submit</button>
            </div>
            <div class="col-md-8">
                <p class="sucessmsg"></p>
            </div>
        </div>
        </div>
    <div class="clearfix"> </div>
</div>
