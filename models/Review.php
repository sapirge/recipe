<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property int $rating
 * @property string $feedback
 * @property int $recipe_id
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rating', 'recipe_id'], 'required'],
            [['rating', 'recipe_id'], 'integer'],
            [['feedback'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rating' => 'Rating',
            'feedback' => 'Feedback',
            'recipe_id' => 'Recipe ID',
        ];
    }
}
