<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "favorite".
 *
 * @property int $id
 * @property int $user_id
 * @property int $recipe_id
 */
class Favorite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'favorite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'recipe_id'], 'required'],
            [['user_id', 'recipe_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'recipe_id' => 'Recipe ID',
        ];
    }
    public static function checkFavExist($recipe_id){
        $user_id = \Yii::$app->user->id;
        $count = Yii::$app->db->createCommand("SELECT count(*) FROM favorite WHERE favorite.recipe_id={$recipe_id}")->queryScalar();
        return $count;
    }
}
