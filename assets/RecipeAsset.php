<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

//use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RecipeAsset extends AppAsset
{
    //public $sourcePath = '@app/themes/recipe';
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $publishOptions = [
    'forceCopy' => true,
];
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/font-awesome.css',
        'css/flexslider.css',
        'css/owl.carousel.min.css',
        '//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic',
        '//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic',
    ];
    public $js = [
        'js/move-top.js',
        'js/easing.js',
        'js/jquery.flexslider.js',
        'js/minicart.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/jquery.validate.js',
        'js/okzoom.js',
        //'js/jquery.fontstar.js',
        'js/jquery.emojiRatings.min.js'
    ];
    /*public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];*/
}
