$(document).ready(function() {
         //var navoffeset=$(".agileits_header").offset().top;
         /*$(window).scroll(function(){
            var scrollpos=$(window).scrollTop(); 
            if(scrollpos >=navoffeset){
                $(".agileits_header").addClass("fixed");
            }else{
                $(".agileits_header").removeClass("fixed");
            }
         });*/
         if(getCookie('isReviewed')=='1')
         {
            $(".write_review_desc").hide();
            $(".write_btn").hide();
         }
         $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
      $('#recipe_img').okzoom({
        width: 150,
        height: 150,
        border: "1px solid black",
        shadow: "0 0 5px #000"
      });  
      $("#rating").emojiRating({
        fontSize: 32,
        onUpdate: function(count) {
          $(".review-text").show();
          $("#starCount").html(count + " Star");
        }
      });
      $("#btn-write-review").click(function(){
        
        $.ajax({
          type:'post',
          data:{recipe_id:$("#recipe_id").val(),rating:$("input[name='reviewStars']:checked").val(),review:$("#review_desc").val()},
          url:$("#write_review_action").val(),
          beforeSend: function(){
            $("#btn-write-review").prop('disabled',true);
          },
          success:function(res){
            if(res==1)
            {
              $(".sucessmsg").text('Your review save successfully!');
              $("#btn-write-review").prop('disabled',true);
              setCookie('isReviewed','1',1);
            }
            else
            {
              $("#btn-write-review").prop('disabled',false);
            }
          },
          error: function(){

          }
        });
      })
    });
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}