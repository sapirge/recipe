<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Recipe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recipe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ingredients')->textarea(['rows' => 6]) ?>

    <?php //= $form->field($model, 'image')->textarea(['rows' => 6]) ?>

  
	<?php echo $form->field($model,'category')->dropdownlist(ArrayHelper::map(Category::find()->all(), 'id', 'name')); ?>

    <?= $form->field($model, 'cooking_time')->textInput(['maxlength' => true]) ?>

    
    <?= $form->field($model, 'posted_by')->hiddenInput(['value'=>\Yii::$app->user->identity->id])->label(false); ?>

    <?= $form->field($model, 'created_date')->hiddenInput(['value'=>date('Y-m-d H:i:s')])->label(false); ?>

   

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
