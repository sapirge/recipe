<?php

use yii\db\Migration;

/**
 * Class m180915_111230_rbac_init
 */
class m180915_111230_rbac_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180915_111230_rbac_init cannot be reverted.\n";

        return false;
    }
	public function up(){
		$auth = Yii::$app->authManager;

        $manageArticles = $auth->createPermission('manageRecipe');
        $manageArticles->description = 'Manage recipe';
        $auth->add($manageArticles);

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);

        $moderator = $auth->createRole('author');
        $moderator->description = 'Author';
        $auth->add($moderator);
        $auth->addChild($moderator, $manageArticles);

        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $auth->add($admin);
        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $manageUsers);
	}
	public function down(){
		echo "m180915_111230_rbac_init cannot be reverted.\n";
		Yii::$app->authManager->removeAll();
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180915_111230_rbac_init cannot be reverted.\n";

        return false;
    }
    */
}
